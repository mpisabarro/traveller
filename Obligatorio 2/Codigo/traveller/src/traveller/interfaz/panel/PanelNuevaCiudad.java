package traveller.interfaz.panel;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import traveller.dominio.Ciudad;
import traveller.dominio.ISistema;

public class PanelNuevaCiudad extends javax.swing.JPanel {

    public PanelNuevaCiudad(PanelAgendarViaje padre, ISistema sistema) {
        initComponents();
        
        lstCiudades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        this.padre = padre;
        this.sistema = sistema;
        this.ciudadesAux = sistema.getListaCiudades();
        
        refrescarLista();
        
        this.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTitulo = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        panelCrearNuevoTipoEvento = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstCiudades = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();
        txtNombreCiudad = new javax.swing.JTextField();
        btnAceptar = new javax.swing.JButton();
        btnAyuda = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(460, 679));

        panelTitulo.setBackground(new java.awt.Color(39, 39, 39));
        panelTitulo.setForeground(new java.awt.Color(255, 255, 255));

        etqTitulo.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqTitulo.setForeground(new java.awt.Color(255, 255, 255));
        etqTitulo.setText("Nueva Ciudad");

        javax.swing.GroupLayout panelTituloLayout = new javax.swing.GroupLayout(panelTitulo);
        panelTitulo.setLayout(panelTituloLayout);
        panelTituloLayout.setHorizontalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo)
                .addContainerGap(310, Short.MAX_VALUE))
        );
        panelTituloLayout.setVerticalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCrearNuevoTipoEvento.setBackground(new java.awt.Color(255, 255, 255));
        panelCrearNuevoTipoEvento.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(39, 39, 39), 1, true));
        panelCrearNuevoTipoEvento.setMaximumSize(new java.awt.Dimension(380, 290));
        panelCrearNuevoTipoEvento.setPreferredSize(new java.awt.Dimension(380, 290));

        lstCiudades.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstCiudades.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lstCiudades.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstCiudadesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstCiudades);

        jLabel1.setText("Ciudades");

        btnVolver.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        btnVolver.setText("Volver");
        btnVolver.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        txtNombreCiudad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNombreCiudad.setMinimumSize(new java.awt.Dimension(2, 21));
        txtNombreCiudad.setPreferredSize(new java.awt.Dimension(2, 21));

        btnAceptar.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btnAceptar.setText("Aceptar");
        btnAceptar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnAyuda.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        btnAyuda.setText("Ayuda");
        btnAyuda.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAyudaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCrearNuevoTipoEventoLayout = new javax.swing.GroupLayout(panelCrearNuevoTipoEvento);
        panelCrearNuevoTipoEvento.setLayout(panelCrearNuevoTipoEventoLayout);
        panelCrearNuevoTipoEventoLayout.setHorizontalGroup(
            panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 361, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addComponent(txtNombreCiudad, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAyuda, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        panelCrearNuevoTipoEventoLayout.setVerticalGroup(
            panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAyuda, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCrearNuevoTipoEvento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelCrearNuevoTipoEvento, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)
                .addGap(105, 105, 105))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lstCiudadesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstCiudadesMouseClicked
        if(estoyModificandoUnaCiudad()){
            String nombreCiudadAModificar = lstCiudades.getSelectedValue().toString();
            txtNombreCiudad.setText(nombreCiudadAModificar);
        }
    }//GEN-LAST:event_lstCiudadesMouseClicked

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        this.setVisible(false);
        guardarNuevasCiudades();
        padre.padre.panelContenido.remove(this);
        padre.padre.pack();
        padre.setVisible(true);
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if(estoyModificandoUnaCiudad()){
            modificoLaCiudad();
        }
        agregoLaCiudad();
        txtNombreCiudad.setText("");
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAyudaActionPerformed
        JOptionPane.showMessageDialog(panelTitulo, "Para modificar una ciudad, "
            + "seleccionarla de la lista. "
            + "\n Luego de modificar, hacer click en Aceptar",
            "Ayuda", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnAyudaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAyuda;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList lstCiudades;
    private javax.swing.JPanel panelCrearNuevoTipoEvento;
    private javax.swing.JPanel panelTitulo;
    private javax.swing.JTextField txtNombreCiudad;
    // End of variables declaration//GEN-END:variables
    private ISistema sistema;
    private PanelAgendarViaje padre;
    private ArrayList<Ciudad> ciudadesAux;
    
    private boolean estoyModificandoUnaCiudad() {
        return lstCiudades.getSelectedIndex() != -1;
    }

    private void modificoLaCiudad() {
        try{
            String nombreCiudad = txtNombreCiudad.getText();
            Ciudad nuevaCiudad = new Ciudad(nombreCiudad);
        
            ciudadesAux.add(nuevaCiudad);
        
            borroViejaCiudad();
            lstCiudades.clearSelection();
        }catch(NullPointerException e){
            String nombreCiudad = txtNombreCiudad.getText();
            Ciudad nuevaCiudad = new Ciudad(nombreCiudad);
        
            ciudadesAux.remove(nuevaCiudad);
        }
    }

    private void agregoLaCiudad() {
        String nombreCiudad = txtNombreCiudad.getText();
        Ciudad ciudad = new Ciudad(nombreCiudad);
        if(nuevaCiudadEsUnico(ciudad)){
            ciudadesAux.add(ciudad);
        }else{
            modificoLaCiudad();
        }
        refrescarLista();
    }

    private void guardarNuevasCiudades() {
        sistema.setCiudades(ciudadesAux);
        padre.recargarCiudades();
    }

    private void refrescarLista() {
        lstCiudades.setListData(ciudadesAux.toArray());
    }

    private void borroViejaCiudad() {
        String nombreViejaCiudad = lstCiudades.getSelectedValue().toString();
        Ciudad viejaCiudad = new Ciudad(nombreViejaCiudad);
        
        ciudadesAux.remove(viejaCiudad);
    }

    private boolean nuevaCiudadEsUnico(Ciudad ciudad) {
        return !ciudadesAux.contains(ciudad);
    }
}
