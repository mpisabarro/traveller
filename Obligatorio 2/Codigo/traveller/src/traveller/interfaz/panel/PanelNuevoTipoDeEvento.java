package traveller.interfaz.panel;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import traveller.dominio.ISistema;
import traveller.dominio.TipoEvento;

public class PanelNuevoTipoDeEvento extends javax.swing.JPanel {

    public PanelNuevoTipoDeEvento(PanelNuevoEvento padre, ISistema sistema) {
        initComponents();

        lstTipos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.padre = padre;
        this.sistema = sistema;
        this.tiposAux = sistema.getListaTiposDeEvento();

        refrescarLista();

        this.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTitulo = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        panelCrearNuevoTipoEvento = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstTipos = new javax.swing.JList();
        jLabel1 = new javax.swing.JLabel();
        btnVolver = new javax.swing.JButton();
        txtNombreTipo = new javax.swing.JTextField();
        btnAceptar = new javax.swing.JButton();
        btnAyuda = new javax.swing.JButton();
        lblErrorNombreTipo = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(460, 679));

        panelTitulo.setBackground(new java.awt.Color(39, 39, 39));
        panelTitulo.setForeground(new java.awt.Color(255, 255, 255));

        etqTitulo.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqTitulo.setForeground(new java.awt.Color(255, 255, 255));
        etqTitulo.setText("Nuevo Tipo de Evento");

        javax.swing.GroupLayout panelTituloLayout = new javax.swing.GroupLayout(panelTitulo);
        panelTitulo.setLayout(panelTituloLayout);
        panelTituloLayout.setHorizontalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo)
                .addContainerGap(251, Short.MAX_VALUE))
        );
        panelTituloLayout.setVerticalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelCrearNuevoTipoEvento.setBackground(new java.awt.Color(255, 255, 255));
        panelCrearNuevoTipoEvento.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(39, 39, 39), 1, true));
        panelCrearNuevoTipoEvento.setMaximumSize(new java.awt.Dimension(380, 290));
        panelCrearNuevoTipoEvento.setPreferredSize(new java.awt.Dimension(380, 290));

        lstTipos.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        lstTipos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lstTipos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstTiposMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstTipos);

        jLabel1.setText("Tipos de evento");

        btnVolver.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        btnVolver.setText("Volver");
        btnVolver.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVolverActionPerformed(evt);
            }
        });

        txtNombreTipo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNombreTipo.setMinimumSize(new java.awt.Dimension(2, 21));
        txtNombreTipo.setPreferredSize(new java.awt.Dimension(2, 21));
        txtNombreTipo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreTipoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreTipoFocusLost(evt);
            }
        });

        btnAceptar.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btnAceptar.setText("Aceptar");
        btnAceptar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnAyuda.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        btnAyuda.setText("Ayuda");
        btnAyuda.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        btnAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAyudaActionPerformed(evt);
            }
        });

        lblErrorNombreTipo.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout panelCrearNuevoTipoEventoLayout = new javax.swing.GroupLayout(panelCrearNuevoTipoEvento);
        panelCrearNuevoTipoEvento.setLayout(panelCrearNuevoTipoEventoLayout);
        panelCrearNuevoTipoEventoLayout.setHorizontalGroup(
            panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 320, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAyuda, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblErrorNombreTipo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtNombreTipo, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
        );
        panelCrearNuevoTipoEventoLayout.setVerticalGroup(
            panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCrearNuevoTipoEventoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblErrorNombreTipo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                .addGroup(panelCrearNuevoTipoEventoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnVolver, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAyuda, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panelTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(panelCrearNuevoTipoEvento, javax.swing.GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelCrearNuevoTipoEvento, javax.swing.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
                .addGap(100, 100, 100))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVolverActionPerformed
        this.setVisible(false);
        guardarNuevosTipos();
        limpiarMensajeDeError();
        padre.padre.panelContenido.remove(this);
        padre.padre.pack();
        padre.setVisible(true);
    }//GEN-LAST:event_btnVolverActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if (!elCampoTieneAlgoEscrito()) {
            if (estoyModificandoUnTipo()) {
                modificoElTipo();
            }
            agregoElNuevoTipo();
            txtNombreTipo.setText("");
            limpiarMensajeDeError();
        }else{
            mostrarMensajeDeError();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAyudaActionPerformed
        JOptionPane.showMessageDialog(panelTitulo, "Para modificar un tipo, "
                + "seleccionarlo de la lista. "
                + "\n Luego de modificar, hacer click en Aceptar",
                "Ayuda", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_btnAyudaActionPerformed

    private void lstTiposMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstTiposMouseClicked
        if (estoyModificandoUnTipo()) {
            limpiarMensajeDeError();
            String nombreTipoAModificar = lstTipos.getSelectedValue().toString();
            txtNombreTipo.setText(nombreTipoAModificar);
        }
    }//GEN-LAST:event_lstTiposMouseClicked

    private void txtNombreTipoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreTipoFocusGained
        limpiarMensajeDeError();
    }//GEN-LAST:event_txtNombreTipoFocusGained

    private void txtNombreTipoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreTipoFocusLost
         if (!elCampoTieneAlgoEscrito()){
             mostrarMensajeDeError();
         }
    }//GEN-LAST:event_txtNombreTipoFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAyuda;
    private javax.swing.JButton btnVolver;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblErrorNombreTipo;
    private javax.swing.JList lstTipos;
    private javax.swing.JPanel panelCrearNuevoTipoEvento;
    private javax.swing.JPanel panelTitulo;
    private javax.swing.JTextField txtNombreTipo;
    // End of variables declaration//GEN-END:variables

    private ISistema sistema;
    private ArrayList<TipoEvento> tiposAux;
    private ArrayList<TipoEvento> tiposModificados;
    private PanelNuevoEvento padre;

    private boolean estoyModificandoUnTipo() {
        return lstTipos.getSelectedIndex() != -1;
    }

    private void modificoElTipo() {
        try {
            String nombreTipo = txtNombreTipo.getText();
            TipoEvento nuevoTipo = new TipoEvento(nombreTipo);

            tiposAux.add(nuevoTipo);

            borroViejoTipo();
            lstTipos.clearSelection();
        } catch (NullPointerException e) {
            String nombreTipo = txtNombreTipo.getText();
            TipoEvento nuevoTipo = new TipoEvento(nombreTipo);

            tiposAux.remove(nuevoTipo);
        }
    }

    private void agregoElNuevoTipo() {
        String nombreTipo = txtNombreTipo.getText();
        TipoEvento tipo = new TipoEvento(nombreTipo);
        if (nuevoTipoEsUnico(tipo)) {
            tiposAux.add(tipo);
        } else {
            modificoElTipo();
        }
        refrescarLista();
    }

    private void refrescarLista() {
        lstTipos.setListData(tiposAux.toArray());
    }

    private boolean nuevoTipoEsUnico(TipoEvento tipo) {
        return !tiposAux.contains(tipo);
    }

    private void guardarNuevosTipos() {
        sistema.setTiposDeEvento(tiposAux);
        padre.recargarTipos();
    }

    private void borroViejoTipo() {
        String nombreViejoTipo = lstTipos.getSelectedValue().toString();
        TipoEvento viejoTipo = new TipoEvento(nombreViejoTipo);

        tiposAux.remove(viejoTipo);
    }

    private boolean elCampoTieneAlgoEscrito() {
        return txtNombreTipo.getText().isEmpty();
    }

    private void mostrarMensajeDeError() {
        lblErrorNombreTipo.setText("El tipo no puede ser vacío");
    }

    private void limpiarMensajeDeError() {
        lblErrorNombreTipo.setText("");
    }
    
}
