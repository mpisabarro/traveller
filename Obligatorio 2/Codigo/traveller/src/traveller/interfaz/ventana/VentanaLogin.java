package traveller.interfaz.ventana;

import traveller.excepciones.usuario.UsuarioException;
import traveller.excepciones.usuario.EmailInvalidoException;
import traveller.excepciones.usuario.NombreVacioException;
import traveller.excepciones.usuario.NombreUsuarioVacioException;
import traveller.excepciones.usuario.LargoApellidoInvalidoException;
import traveller.excepciones.usuario.LargoClaveInvalidoException;
import traveller.excepciones.usuario.ApellidoVacioException;
import traveller.excepciones.usuario.LargoNombreInvalidoException;
import traveller.excepciones.usuario.LargoNombreUsuarioInvalidoException;
import traveller.excepciones.usuario.ClaveInseguraException;
import traveller.dominio.Email;
import traveller.dominio.ISistema;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.border.LineBorder;
import traveller.dominio.Usuario;

public class VentanaLogin extends javax.swing.JFrame {

    public VentanaLogin(ISistema sistema) {
        this.sistema = sistema;

        agregarIconoDeLaVentana();

        initComponents();

        cargarCampos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelPrincipal = new javax.swing.JPanel();
        panelTitulo = new javax.swing.JPanel();
        etqTitulo = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        etqUsuario = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        etqContraseña = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        etqErrorLogin = new javax.swing.JLabel();
        pwdContraseñaLogin = new javax.swing.JPasswordField();
        lblErrorLoginUsuario = new javax.swing.JLabel();
        lblErrorLoginContrasena = new javax.swing.JLabel();
        panelTitulo1 = new javax.swing.JPanel();
        etqTitulo1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        etqNormbreRegistro = new javax.swing.JLabel();
        txtEmailRegistro = new javax.swing.JTextField();
        etqEmailRegistro = new javax.swing.JLabel();
        txtApellidoRegistro = new javax.swing.JTextField();
        btnAceptarRegistro = new javax.swing.JButton();
        etqErrorRegistro = new javax.swing.JLabel();
        txtNombreRegistro = new javax.swing.JTextField();
        etqUsuarioRegistro = new javax.swing.JLabel();
        txtUsuarioRegistro = new javax.swing.JTextField();
        etqContraseñaRegistro = new javax.swing.JLabel();
        pwdContraseñaRegistro = new javax.swing.JPasswordField();
        etqCorrecto = new javax.swing.JLabel();
        lblErrorNombre = new javax.swing.JLabel();
        lblErrorApellido = new javax.swing.JLabel();
        lblErrorUsuario = new javax.swing.JLabel();
        lblErrorContrasena = new javax.swing.JLabel();
        lblErrorEmail = new javax.swing.JLabel();
        imgLogo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Viajenda");
        setLocationByPlatform(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        panelPrincipal.setBackground(new java.awt.Color(255, 255, 255));
        panelPrincipal.setVerifyInputWhenFocusTarget(false);

        panelTitulo.setBackground(new java.awt.Color(39, 39, 39));
        panelTitulo.setForeground(new java.awt.Color(255, 255, 255));
        panelTitulo.setMaximumSize(new java.awt.Dimension(390, 46));

        etqTitulo.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqTitulo.setForeground(new java.awt.Color(255, 255, 255));
        etqTitulo.setText("Login");

        javax.swing.GroupLayout panelTituloLayout = new javax.swing.GroupLayout(panelTitulo);
        panelTitulo.setLayout(panelTituloLayout);
        panelTituloLayout.setHorizontalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelTituloLayout.setVerticalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(245, 245, 245));
        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(39, 39, 39), 1, true));

        etqUsuario.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqUsuario.setText("Usuario:");

        txtUsuario.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        txtUsuario.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        txtUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUsuarioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtUsuarioFocusLost(evt);
            }
        });
        txtUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUsuario.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtUsuario.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        txtUsuario.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!txtUsuario.isFocusOwner()) {
                    txtUsuario.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!txtUsuario.isFocusOwner()) {
                    txtUsuario.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });

        etqContraseña.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqContraseña.setText("Contraseña:");

        btnAceptar.setBackground(new java.awt.Color(255, 102, 51));
        btnAceptar.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btnAceptar.setForeground(new java.awt.Color(102, 102, 102));
        btnAceptar.setText("Acceder");
        btnAceptar.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(221, 221, 221), 1, true));
        btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnAceptar.setFocusPainted(false);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        etqErrorLogin.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        etqErrorLogin.setForeground(new java.awt.Color(204, 0, 0));
        etqErrorLogin.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        pwdContraseñaLogin.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        pwdContraseñaLogin.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pwdContraseñaLogin.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pwdContraseñaLogin.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        pwdContraseñaLogin.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!pwdContraseñaLogin.isFocusOwner()) {
                    pwdContraseñaLogin.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!pwdContraseñaLogin.isFocusOwner()) {
                    pwdContraseñaLogin.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });
        pwdContraseñaLogin.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pwdContraseñaLoginFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pwdContraseñaLoginFocusLost(evt);
            }
        });

        lblErrorLoginUsuario.setForeground(new java.awt.Color(255, 0, 0));

        lblErrorLoginContrasena.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(69, Short.MAX_VALUE)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblErrorLoginContrasena, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(etqErrorLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(etqContraseña)
                            .addComponent(etqUsuario))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUsuario)
                            .addComponent(pwdContraseñaLogin, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addComponent(lblErrorLoginUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqErrorLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etqUsuario))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblErrorLoginUsuario)
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etqContraseña)
                    .addComponent(pwdContraseñaLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblErrorLoginContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        panelTitulo1.setBackground(new java.awt.Color(39, 39, 39));
        panelTitulo1.setForeground(new java.awt.Color(255, 255, 255));
        panelTitulo1.setMaximumSize(new java.awt.Dimension(390, 46));

        etqTitulo1.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqTitulo1.setForeground(new java.awt.Color(255, 255, 255));
        etqTitulo1.setText("Registro");

        javax.swing.GroupLayout panelTitulo1Layout = new javax.swing.GroupLayout(panelTitulo1);
        panelTitulo1.setLayout(panelTitulo1Layout);
        panelTitulo1Layout.setHorizontalGroup(
            panelTitulo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTitulo1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelTitulo1Layout.setVerticalGroup(
            panelTitulo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTitulo1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etqTitulo1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(245, 245, 245));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(39, 39, 39), 1, true));

        etqNormbreRegistro.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqNormbreRegistro.setText("Nombre:");

        txtEmailRegistro.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        txtEmailRegistro.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        txtEmailRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtEmailRegistroFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtEmailRegistroFocusLost(evt);
            }
        });
        txtEmailRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtEmailRegistro.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtEmailRegistro.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        txtEmailRegistro.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!txtEmailRegistro.isFocusOwner()) {
                    txtEmailRegistro.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!txtEmailRegistro.isFocusOwner()) {
                    txtEmailRegistro.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });

        etqEmailRegistro.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqEmailRegistro.setText("E-mail:");

        txtApellidoRegistro.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        txtApellidoRegistro.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        txtApellidoRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtApellidoRegistroFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtApellidoRegistroFocusLost(evt);
            }
        });
        txtApellidoRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtApellidoRegistro.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtApellidoRegistro.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        txtApellidoRegistro.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!txtApellidoRegistro.isFocusOwner()) {
                    txtApellidoRegistro.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!txtApellidoRegistro.isFocusOwner()) {
                    txtApellidoRegistro.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });

        btnAceptarRegistro.setBackground(new java.awt.Color(242, 242, 242));
        btnAceptarRegistro.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        btnAceptarRegistro.setForeground(new java.awt.Color(102, 102, 102));
        btnAceptarRegistro.setText("Registrar");
        btnAceptarRegistro.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(221, 221, 221), 1, true));
        btnAceptarRegistro.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnAceptarRegistro.setFocusPainted(false);
        btnAceptarRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarRegistroActionPerformed(evt);
            }
        });

        etqErrorRegistro.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        etqErrorRegistro.setForeground(new java.awt.Color(204, 0, 0));
        etqErrorRegistro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        txtNombreRegistro.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        txtNombreRegistro.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        txtNombreRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreRegistroFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreRegistroFocusLost(evt);
            }
        });
        txtNombreRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreRegistro.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreRegistro.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        txtNombreRegistro.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!txtUsuarioRegistro.isFocusOwner()) {
                    txtNombreRegistro.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!txtUsuarioRegistro.isFocusOwner()) {
                    txtNombreRegistro.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });

        etqUsuarioRegistro.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqUsuarioRegistro.setText("Usuario:");

        txtUsuarioRegistro.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        txtUsuarioRegistro.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        txtUsuarioRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUsuarioRegistroFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtUsuarioRegistroFocusLost(evt);
            }
        });
        txtUsuarioRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUsuarioRegistro.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtUsuarioRegistro.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        txtUsuarioRegistro.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!txtUsuarioRegistro.isFocusOwner()) {
                    txtUsuarioRegistro.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!txtUsuarioRegistro.isFocusOwner()) {
                    txtUsuarioRegistro.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });

        etqContraseñaRegistro.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etqContraseñaRegistro.setText("Contraseña:");

        pwdContraseñaRegistro.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(217, 217, 217), 1, true));
        pwdContraseñaRegistro.setMinimumSize(new java.awt.Dimension(2, 21));
        pwdContraseñaRegistro.setPreferredSize(new java.awt.Dimension(2, 21));
        pwdContraseñaRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pwdContraseñaRegistroFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pwdContraseñaRegistroFocusLost(evt);
            }
        });
        pwdContraseñaRegistro.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pwdContraseñaRegistro.setBorder(new LineBorder(new Color(77,144,254)));
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                pwdContraseñaRegistro.setBorder(new LineBorder(new Color(217,217,217)));
            }
        });
        pwdContraseñaRegistro.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseEntered(java.awt.event.MouseEvent evt) {
                if (!pwdContraseñaRegistro.isFocusOwner()) {
                    pwdContraseñaRegistro.setBorder(new LineBorder(new Color(185, 185, 185)));
                }
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                if (!pwdContraseñaRegistro.isFocusOwner()) {
                    pwdContraseñaRegistro.setBorder(new LineBorder(new Color(217, 217, 217)));
                }
            }
        });

        etqCorrecto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        etqCorrecto.setForeground(new java.awt.Color(0, 153, 0));

        lblErrorNombre.setForeground(new java.awt.Color(255, 0, 51));

        lblErrorApellido.setForeground(new java.awt.Color(255, 51, 51));

        lblErrorUsuario.setForeground(new java.awt.Color(255, 0, 0));

        lblErrorContrasena.setForeground(new java.awt.Color(255, 51, 51));

        lblErrorEmail.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etqNormbreRegistro)
                    .addComponent(etqEmailRegistro)
                    .addComponent(etqContraseñaRegistro)
                    .addComponent(etqCorrecto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(etqErrorRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombreRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etqUsuarioRegistro)
                            .addComponent(lblErrorNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtApellidoRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                            .addComponent(lblErrorApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(lblErrorEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblErrorContrasena, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblErrorUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEmailRegistro)
                    .addComponent(pwdContraseñaRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtUsuarioRegistro, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAceptarRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(90, 90, 90))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(etqErrorRegistro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etqNormbreRegistro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtApellidoRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblErrorNombre)
                    .addComponent(lblErrorApellido))
                .addGap(1, 1, 1)
                .addComponent(etqUsuarioRegistro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUsuarioRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblErrorUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etqContraseñaRegistro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pwdContraseñaRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblErrorContrasena)
                .addGap(2, 2, 2)
                .addComponent(etqEmailRegistro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEmailRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblErrorEmail)
                .addGap(22, 22, 22)
                .addComponent(etqCorrecto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(btnAceptarRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );

        imgLogo.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        imgLogo.setForeground(new java.awt.Color(204, 204, 204));
        imgLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/traveller/interfaz/img/AvionInicio.png"))); // NOI18N
        imgLogo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(imgLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelTitulo1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelTitulo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(imgLogo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        guardarSistema();
    }//GEN-LAST:event_formWindowClosing

    private void btnAceptarRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarRegistroActionPerformed
        validarQueSeHayanCompletadoLosCamposDeNombreYApellido();
        String usuario = txtUsuarioRegistro.getText();
        String contraseña = pwdContraseñaRegistro.getText();
        try {
            crearUsuario();

            iniciarSesion(usuario, contraseña);

            crearVentanaMenu();
        } catch (UsuarioException e) {
            borrarCamposNoCompletos();

            mostrarErrorResgistro(e.toString());

            hacerFocoEnLaFuenteDelError(e);
        }
    }//GEN-LAST:event_btnAceptarRegistroActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        String usuario = txtUsuario.getText();
        String contraseña = pwdContraseñaLogin.getText();
        try {
            iniciarSesion(usuario, contraseña);

            crearVentanaMenu();
        } catch (UsuarioException e) {
            etqErrorLogin.setText(e.toString());
            txtUsuario.requestFocusInWindow();
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void txtApellidoRegistroFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtApellidoRegistroFocusGained
        if (txtApellidoRegistro.getText().equals("Apellido")) {
            txtApellidoRegistro.setText("");
        }
        lblErrorApellido.setText("");
        txtApellidoRegistro.setForeground(Color.black);
    }//GEN-LAST:event_txtApellidoRegistroFocusGained

    private void txtNombreRegistroFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreRegistroFocusGained
        if (txtNombreRegistro.getText().equals("Nombre")) {
            txtNombreRegistro.setText("");
        }
        lblErrorNombre.setText("");
        txtNombreRegistro.setForeground(Color.black);
    }//GEN-LAST:event_txtNombreRegistroFocusGained

    private void txtNombreRegistroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreRegistroFocusLost
        String texto = txtNombreRegistro.getText();
        if (texto.isEmpty()) {
            txtNombreRegistro.setForeground(Color.GRAY);
            lblErrorNombre.setText("El nombre no puede ser vacío");
        } else if (texto.length() < Usuario.LARGO_MINIMO_NOMBRE) {
            lblErrorNombre.setText("El nombre no puede ser menor a "
                    + Usuario.LARGO_MINIMO_NOMBRE + " caracteres");
        } else if (texto.length() > Usuario.LARGO_MAXIMO_NOMBRE) {
            lblErrorNombre.setText("El nombre no puede ser mayor a "
                    + Usuario.LARGO_MAXIMO_NOMBRE + " caracteres");
        }
    }//GEN-LAST:event_txtNombreRegistroFocusLost

    private void txtApellidoRegistroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtApellidoRegistroFocusLost
        String texto = txtApellidoRegistro.getText();
        if (texto.isEmpty()) {
            txtApellidoRegistro.setForeground(Color.GRAY);
            lblErrorApellido.setText("El apellido no puede ser vacío");
        } else if (texto.length() < Usuario.LARGO_MINIMO_APELLIDO) {
            lblErrorApellido.setText("El apellido no puede ser menor a "
                    + Usuario.LARGO_MINIMO_APELLIDO + " caracteres");
        } else if (texto.length() > Usuario.LARGO_MAXIMO_APELLIDO) {
            lblErrorApellido.setText("El apellido no puede ser mayor a "
                    + Usuario.LARGO_MAXIMO_APELLIDO + " caracteres");
        }
    }//GEN-LAST:event_txtApellidoRegistroFocusLost

    private void txtUsuarioRegistroFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsuarioRegistroFocusGained
        lblErrorUsuario.setText("");
        txtUsuarioRegistro.setForeground(Color.black);
    }//GEN-LAST:event_txtUsuarioRegistroFocusGained

    private void txtUsuarioRegistroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsuarioRegistroFocusLost
        String texto = txtUsuarioRegistro.getText();
        if (texto.isEmpty()) {
            txtUsuarioRegistro.setForeground(Color.GRAY);
            lblErrorUsuario.setText("El usuario no puede ser vacío");
        } else if (texto.length() < Usuario.LARGO_MINIMO_USUARIO) {
            lblErrorUsuario.setText("El usuario no puede ser menor a "
                    + Usuario.LARGO_MINIMO_USUARIO + " caracteres");
        } else if (texto.length() > Usuario.LARGO_MAXIMO_USUARIO) {
            lblErrorUsuario.setText("El usuario no puede ser mayor a "
                    + Usuario.LARGO_MAXIMO_USUARIO + " caracteres");
        }
    }//GEN-LAST:event_txtUsuarioRegistroFocusLost

    private void pwdContraseñaRegistroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pwdContraseñaRegistroFocusLost
        String texto = pwdContraseñaRegistro.getText();
        if (texto.isEmpty()) {
            pwdContraseñaRegistro.setForeground(Color.GRAY);
            lblErrorContrasena.setText("La contraseña no puede ser vacía");
        } else if (texto.length() > Usuario.LARGO_MAXIMO_CONTRASENA) {
            lblErrorContrasena.setText("La contraseña no puede tener mas de "
                    + Usuario.LARGO_MAXIMO_CONTRASENA + " caracteres");
        }
    }//GEN-LAST:event_pwdContraseñaRegistroFocusLost

    private void pwdContraseñaRegistroFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pwdContraseñaRegistroFocusGained
        lblErrorContrasena.setText("");
        pwdContraseñaRegistro.setForeground(Color.black);
    }//GEN-LAST:event_pwdContraseñaRegistroFocusGained

    private void txtEmailRegistroFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEmailRegistroFocusGained
        lblErrorEmail.setText("");
        txtEmailRegistro.setForeground(Color.black);
    }//GEN-LAST:event_txtEmailRegistroFocusGained

    private void txtEmailRegistroFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtEmailRegistroFocusLost
        String texto = txtEmailRegistro.getText();
        if (texto.isEmpty()) {
            txtEmailRegistro.setForeground(Color.GRAY);
            lblErrorEmail.setText("El email no puede ser vacío");
        }
    }//GEN-LAST:event_txtEmailRegistroFocusLost

    private void txtUsuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsuarioFocusGained
        lblErrorLoginUsuario.setText("");
        txtUsuario.setForeground(Color.black);
    }//GEN-LAST:event_txtUsuarioFocusGained

    private void txtUsuarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsuarioFocusLost
        String texto = txtUsuario.getText();
        if (texto.isEmpty()) {
            txtUsuario.setForeground(Color.GRAY);
            lblErrorLoginUsuario.setText("El usuario no puede ser vacío");
        } else if (texto.length() < Usuario.LARGO_MINIMO_USUARIO) {
            lblErrorLoginUsuario.setText("El usuario no puede ser menor a "
                    + Usuario.LARGO_MINIMO_USUARIO + " caracteres");
        } else if (texto.length() > Usuario.LARGO_MAXIMO_USUARIO) {
            lblErrorLoginUsuario.setText("El usuario no puede ser mayor a "
                    + Usuario.LARGO_MAXIMO_USUARIO + " caracteres");
        }
    }//GEN-LAST:event_txtUsuarioFocusLost

    private void pwdContraseñaLoginFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pwdContraseñaLoginFocusGained
        lblErrorLoginContrasena.setText("");
        pwdContraseñaLogin.setForeground(Color.black);
    }//GEN-LAST:event_pwdContraseñaLoginFocusGained

    private void pwdContraseñaLoginFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pwdContraseñaLoginFocusLost
        String texto = pwdContraseñaLogin.getText();
        if (texto.isEmpty()) {
            pwdContraseñaLogin.setForeground(Color.GRAY);
            lblErrorLoginContrasena.setText("La contraseña no puede ser vacía");
        } else if (texto.length() > Usuario.LARGO_MAXIMO_CONTRASENA) {
            lblErrorLoginContrasena.setText("La contraseña no puede tener mas de "
                    + Usuario.LARGO_MAXIMO_CONTRASENA + " caracteres");
        }
    }//GEN-LAST:event_pwdContraseñaLoginFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnAceptarRegistro;
    private javax.swing.JLabel etqContraseña;
    private javax.swing.JLabel etqContraseñaRegistro;
    private javax.swing.JLabel etqCorrecto;
    private javax.swing.JLabel etqEmailRegistro;
    private javax.swing.JLabel etqErrorLogin;
    private javax.swing.JLabel etqErrorRegistro;
    private javax.swing.JLabel etqNormbreRegistro;
    private javax.swing.JLabel etqTitulo;
    private javax.swing.JLabel etqTitulo1;
    private javax.swing.JLabel etqUsuario;
    private javax.swing.JLabel etqUsuarioRegistro;
    private javax.swing.JLabel imgLogo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblErrorApellido;
    private javax.swing.JLabel lblErrorContrasena;
    private javax.swing.JLabel lblErrorEmail;
    private javax.swing.JLabel lblErrorLoginContrasena;
    private javax.swing.JLabel lblErrorLoginUsuario;
    private javax.swing.JLabel lblErrorNombre;
    private javax.swing.JLabel lblErrorUsuario;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JPanel panelTitulo;
    private javax.swing.JPanel panelTitulo1;
    private javax.swing.JPasswordField pwdContraseñaLogin;
    private javax.swing.JPasswordField pwdContraseñaRegistro;
    private javax.swing.JTextField txtApellidoRegistro;
    private javax.swing.JTextField txtEmailRegistro;
    private javax.swing.JTextField txtNombreRegistro;
    private javax.swing.JTextField txtUsuario;
    private javax.swing.JTextField txtUsuarioRegistro;
    // End of variables declaration//GEN-END:variables
    private final ISistema sistema;

    private void guardarSistema() {
        try {
            SerializarSistema();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void agregarIconoDeLaVentana() {
        BufferedImage image;
        try {
            image = ImageIO.read(getClass().getResource("/traveller/interfaz/img/icono.png"));
            setIconImage(image);
        } catch (IOException e) {
        }
    }

    private void cargarCampos() {
        if (this.sistema.esModoDesarrollador()) {
            txtUsuario.requestFocusInWindow();
            txtUsuario.setText("Angus");
            this.pwdContraseñaLogin.setText("angus2012");

            txtNombreRegistro.setText("John Henry");
            txtApellidoRegistro.setText("Bonham");
            txtUsuarioRegistro.setText("Bonzo");
            this.pwdContraseñaRegistro.setText("zosozepp73");
            this.txtEmailRegistro.setText("bonzo.zepp@gmail.com");
        } else {
            txtUsuario.requestFocusInWindow();
            txtNombreRegistro.setText("Nombre");
            txtNombreRegistro.setForeground(Color.GRAY);
            txtApellidoRegistro.setText("Apellido");
            txtApellidoRegistro.setForeground(Color.GRAY);
        }
    }

    private void iniciarSesion(String usuario, String contraseña) throws UsuarioException {
        sistema.identificacionUsuario(usuario, contraseña);
    }

    private void crearVentanaMenu() {
        VentanaMenu ventanaMenu = new VentanaMenu(sistema, txtUsuario.getText());
        ventanaMenu.setLocationRelativeTo(null); // Centrar ventana.
        ventanaMenu.setVisible(true);
        this.setVisible(false);
    }

    private void validarQueSeHayanCompletadoLosCamposDeNombreYApellido() {
        if (txtNombreRegistro.getText().equals("Nombre")) {
            txtNombreRegistro.setText("");
        }
        if (txtApellidoRegistro.getText().equals("Apellido")) {
            txtApellidoRegistro.setText("");
        }
    }

    private void crearUsuario() throws UsuarioException {
        String nombre = txtNombreRegistro.getText();
        String apellido = txtApellidoRegistro.getText();
        String usuario = txtUsuarioRegistro.getText();
        String contraseña = pwdContraseñaRegistro.getText();
        Email email = new Email(txtEmailRegistro.getText());

        sistema.altaUsuario(usuario, contraseña, email, nombre, apellido);
    }

    private void mostrarErrorResgistro(String texto) {
        etqErrorRegistro.setText(texto);
    }

    private void hacerFocoEnLaFuenteDelError(UsuarioException e) {
        if (e.getClass() == ClaveInseguraException.class || e.getClass() == LargoClaveInvalidoException.class) {
            pwdContraseñaRegistro.requestFocusInWindow();
        } else if (e.getClass() == EmailInvalidoException.class) {
            txtEmailRegistro.requestFocusInWindow();
        } else if (e.getClass() == LargoApellidoInvalidoException.class || e.getClass() == ApellidoVacioException.class) {
            txtApellidoRegistro.requestFocusInWindow();
        } else if (e.getClass() == LargoNombreInvalidoException.class || e.getClass() == NombreVacioException.class) {
            txtNombreRegistro.requestFocusInWindow();
        } else if (e.getClass() == LargoNombreUsuarioInvalidoException.class || e.getClass() == NombreUsuarioVacioException.class) {
            txtUsuarioRegistro.requestFocusInWindow();
        }
    }

    private void borrarCamposNoCompletos() {
        if (txtNombreRegistro.getText().isEmpty()) {
            txtNombreRegistro.setText("Nombre");
        }
        if (txtApellidoRegistro.getText().isEmpty()) {
            txtApellidoRegistro.setText("Apellido");
        }
    }

    private void SerializarSistema() throws IOException {
        ObjectOutputStream grabacionSistema;
        grabacionSistema = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream("sistema.txt")));
        grabacionSistema.writeObject(sistema);
        grabacionSistema.close();
    }
}
