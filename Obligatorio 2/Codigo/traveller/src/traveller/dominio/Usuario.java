package traveller.dominio;

import traveller.excepciones.usuario.*;
import traveller.excepciones.fecha.FechaVaciaException;
import traveller.excepciones.fecha.FechaInicioAnteriorException;
import traveller.excepciones.fecha.FechaException;
import traveller.excepciones.fecha.FechaFinAnteriorInicioException;
import traveller.excepciones.fecha.FormatoFechaInicioException;
import traveller.excepciones.fecha.FormatoFechaFinException;
import traveller.excepciones.viaje.ViajeException;
import traveller.excepciones.viaje.ViajeExistenteException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import traveller.excepciones.usuario.ContrasenaVaciaException;
import traveller.utill.Utilidades;

public final class Usuario implements Serializable {

    private String nombreUsuario;
    private String nombre;
    private String contraseña;
    private String apellido;
    private Email email;
    private ArrayList<Viaje> listaViajes;
    private ArrayList<Usuario> listaAmigos;
    private boolean notificarEmail;

    public static int LARGO_MAXIMO_CONTRASENA = 25;
    public static int LARGO_MINIMO_CONTRASENA = 8;
    public static int LARGO_MINIMO_NOMBRE = 2;
    public static int LARGO_MAXIMO_NOMBRE = 20;
    public static int LARGO_MINIMO_APELLIDO = 2;
    public static int LARGO_MAXIMO_APELLIDO = 20;
    public static int LARGO_MINIMO_USUARIO = 4;
    public static int LARGO_MAXIMO_USUARIO = 20;

    public Usuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Usuario(String nombreUsuario, String contraseña, String nombre, String apellido, Email email) throws UsuarioException {
        this.setNombreUsuario(nombreUsuario);
        this.setContraseña(contraseña);
        this.setNombre(nombre);
        this.setApellido(apellido);
        this.setEmail(email);
        this.listaViajes = new ArrayList<Viaje>();
        this.listaAmigos = new ArrayList<Usuario>();
        this.notificarEmail = false;
    }

    public ArrayList<Usuario> listadoAmigosEnComun(Usuario amigo) {
        ArrayList<Usuario> retorno = new ArrayList<Usuario>();

        Iterator<Usuario> it = this.getListaAmigos().iterator();
        while (it.hasNext()) {
            Usuario aux = it.next();
            if (amigo.getListaAmigos().contains(aux)) {
                retorno.add(aux);
            }
        }

        return retorno;
    }

    public ArrayList<Usuario> listadoBuscarAmigos(String cadena) {
        ArrayList<Usuario> retorno = new ArrayList<Usuario>();

        Iterator<Usuario> it = this.getListaAmigos().iterator();
        while (it.hasNext()) {
            Usuario usuario = it.next();
            if ((usuario.getNombre() + " " + usuario.getApellido()).toLowerCase().startsWith(cadena.toLowerCase())) {
                retorno.add(usuario);
            }
        }

        return retorno;
    }

    public boolean isNotificarEmail() {
        return notificarEmail;
    }

    public void setNotificarEmail(boolean notificarEmail) {
        this.notificarEmail = notificarEmail;
    }

    public ArrayList<Usuario> getListaAmigos() {
        return this.listaAmigos;
    }

    public boolean existeNombreViaje(String nombreViaje) {
        return this.getListaViajes().contains(new Viaje(nombreViaje));
    }

    public ArrayList<Viaje> getListaViajes() {
        return this.listaViajes;
    }

    public void bajaViaje(Viaje viaje) {
        this.listaViajes.remove(viaje);
    }

    public void altaViaje(String nombreViaje, Ciudad ciudad, String diaIni, String mesIni, String anioIni,
            String diaFin, String mesFin, String anioFin, String descripcion)
            throws ViajeException, FechaException {

        validarNombreViaje(nombreViaje);

        validarFecha(diaIni, mesIni, anioIni, diaFin, mesFin, anioFin);

        Date fechaInicio = crearFecha(diaIni, mesIni, anioIni);
        Date fechaFin = crearFecha(diaFin, mesFin, anioFin);

        validarContinuidadTemporal(fechaInicio, fechaFin);

        Viaje nuevoViaje = new Viaje(nombreViaje, ciudad, fechaInicio, fechaFin, descripcion);
        this.agregarViajes(nuevoViaje);
    }

    public boolean igualContraseña(String contraseña) {
        return Utilidades.md5(contraseña).equals(this.contraseña);
    }

    public void setContraseña(String clave) throws UsuarioException {
        if (!Utilidades.largoValido(clave, LARGO_MINIMO_CONTRASENA, LARGO_MAXIMO_CONTRASENA)) {
            throw new LargoClaveInvalidoException(LARGO_MINIMO_CONTRASENA, LARGO_MAXIMO_CONTRASENA);
        } else if (clave.isEmpty()) {
            throw new ContrasenaVaciaException();
        } else if (!Utilidades.esAlfanumerico(clave)) {
            throw new ClaveInseguraException();
        } else {
            this.contraseña = Utilidades.md5(clave);
        }
    }

    public void agregarViajes(Viaje viaje) {
        this.listaViajes.add(viaje);
    }

    public void agregarAmigo(Usuario usuario) {
        this.listaAmigos.add(usuario);
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) throws UsuarioException {
        if (apellido.isEmpty()) {
            throw new ApellidoVacioException();
        }
        if (tieneCaracteresEspeciales(apellido)) {
            throw new ApellidoConCaracteresEspecialesException();
        }
        if (!Utilidades.largoValido(apellido, LARGO_MINIMO_APELLIDO, LARGO_MAXIMO_APELLIDO)) {
            throw new LargoApellidoInvalidoException(LARGO_MINIMO_APELLIDO, LARGO_MAXIMO_APELLIDO);
        }
        this.apellido = apellido;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) throws UsuarioException {
        if (!Utilidades.emailValido(email)) {
            throw new EmailInvalidoException();
        }
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) throws UsuarioException {
        if (nombre.isEmpty()) {
            throw new NombreVacioException();
        }
        if (tieneCaracteresEspeciales(nombre)) {
            throw new NombreConCaracteresEspecialesException();
        }
        if (!Utilidades.largoValido(nombre, LARGO_MINIMO_NOMBRE, LARGO_MAXIMO_NOMBRE)) {
            throw new LargoNombreInvalidoException(LARGO_MINIMO_NOMBRE, LARGO_MAXIMO_NOMBRE);
        }
        this.nombre = nombre;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) throws UsuarioException {
        if (nombreUsuario.isEmpty()) {
            throw new NombreUsuarioVacioException();
        }
        if (!Utilidades.largoValido(nombreUsuario, LARGO_MINIMO_USUARIO, LARGO_MAXIMO_USUARIO)) {
            throw new LargoNombreUsuarioInvalidoException(LARGO_MINIMO_USUARIO, LARGO_MAXIMO_USUARIO);
        }

        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getNombreUsuario().equals(((Usuario) obj).getNombreUsuario());
    }

    @Override
    public String toString() {
        return nombre + " " + apellido;
    }

    private void validarNombreViaje(String nombreViaje) throws ViajeExistenteException {
        if (existeNombreViaje(nombreViaje)) {
            throw new ViajeExistenteException();
        }
    }

    private void validarFecha(String diaIni, String mesIni, String anioIni,
            String diaFin, String mesFin, String anioFin) throws FechaVaciaException,
            FormatoFechaInicioException, FormatoFechaFinException,
            FechaInicioAnteriorException, FechaFinAnteriorInicioException {

        validarQueFechaNoSeaVacia(diaIni, mesIni, anioIni, diaFin, mesFin, anioFin);

        validarFormato(diaIni, mesIni, anioIni, diaFin, mesFin, anioFin);
    }

    private void validarQueFechaNoSeaVacia(String diaIni, String mesIni, String anioIni, String diaFin, String mesFin, String anioFin) throws FechaVaciaException {
        if (diaIni.isEmpty() || diaFin.isEmpty() || mesIni.isEmpty()
                || mesFin.isEmpty() || anioIni.isEmpty() || anioFin.isEmpty()) {
            throw new FechaVaciaException();
        }
    }

    private void validarFormato(String diaIni, String mesIni, String anioIni, String diaFin, String mesFin, String anioFin) throws FormatoFechaInicioException, FormatoFechaFinException {
        if (!Utilidades.formatoFechaValido(diaIni, mesIni, anioIni)) {
            throw new FormatoFechaInicioException();
        }
        if (!Utilidades.formatoFechaValido(diaFin, mesFin, anioFin)) {
            throw new FormatoFechaFinException();
        }
    }

    private Date crearFecha(String dia, String mes, String anio) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return formatter.parse(Integer.parseInt(dia) + "/" + Integer.parseInt(mes) + "/" + Integer.parseInt(anio));
        } catch (ParseException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void validarContinuidadTemporal(Date fechaInicio, Date fechaFin) throws FechaInicioAnteriorException, FechaFinAnteriorInicioException {
        Date today = Calendar.getInstance().getTime();

        if (fechaInicio.before(today)) {
            throw new FechaInicioAnteriorException();
        }
        if (fechaFin.before(fechaInicio)) {
            throw new FechaFinAnteriorInicioException();
        }
    }

    private boolean tieneCaracteresEspeciales(String nombre) {
        for (int i = 0; i < nombre.length(); i++) {
            if (esCaracterEspecial(nombre.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private boolean esCaracterEspecial(char charAt) {
        return (charAt < 65 && charAt != 32) || charAt > 122 || (charAt > 90 && charAt < 90);
    }

}
