package traveller.dominio;

import traveller.excepciones.usuario.UsuarioException;
import java.util.ArrayList;
import traveller.excepciones.evento.NoExisteTipoEventoException;
import traveller.excepciones.viaje.NoExisteCiudadException;

public interface ISistema {

    public void altaUsuario(String nombreUsuario, String clave, Email email, String nombre, String apellido)
            throws UsuarioException;

    public void identificacionUsuario(String nombreUsuario, String clave) throws UsuarioException;

    public void logoutUsuario();

    public boolean hayUsuarioIdentificado();

    public void bajaUsuario();

    public ArrayList<Usuario> getListaUsuarios();
    
    public void setTiposDeEvento(ArrayList<TipoEvento> tipos);
    
    public void addTipoDeEvento(TipoEvento tipoDeEvento);
    
    public TipoEvento getTipoEvento(String nombreTipoEvento) throws NoExisteTipoEventoException;
    
    public ArrayList<TipoEvento> getListaTiposDeEvento();
    
    public ArrayList<String> getListaNombresDeTipoDeEvento();

    public boolean existeUsuario(String nombreUsuario);

    public Usuario getUsuarioIdentificado();

    public ArrayList<Usuario> listadoBuscarUsuarios(String cadena);

    public ArrayList<Usuario> obtenerGrupoDeViaje(Viaje viaje);
            
    public void setModoDesarrollador(boolean modoDesarrollador);

    public boolean esModoDesarrollador();
    
    public boolean isMostrarCartelInicio();
     
    public void setMostrarCartelInicio(boolean mostrar);
    
    public void notificarNovedad(String novedadEmail, String titulo, Viaje viaje);
    
    public void identificar(Usuario usuario);
    
    public void addCiudad(Ciudad ciudad);
    
    public ArrayList<Ciudad> getListaCiudades();
    
    public Ciudad getCiudad(String nombreCiudad) throws NoExisteCiudadException;
    
    public void setCiudades(ArrayList<Ciudad> ciudades);
}
