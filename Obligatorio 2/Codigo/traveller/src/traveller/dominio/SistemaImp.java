package traveller.dominio;

import traveller.excepciones.usuario.UsuarioException;
import traveller.excepciones.usuario.IdentificacionInvalidaException;
import traveller.excepciones.usuario.UsuarioExistenteException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import traveller.excepciones.evento.NoExisteTipoEventoException;
import traveller.excepciones.viaje.NoExisteCiudadException;
import traveller.noticiaciones.email.EnvioMail;

public class SistemaImp implements ISistema, Serializable {

    private final ArrayList<Usuario> listaUsuarios;
    private ArrayList<TipoEvento> listaTiposDeEvento;
    private ArrayList<Ciudad> listaCiudades;
    private Usuario usuarioIdentificado;
    private boolean modoDesarrollador;
    private boolean mostrarCartelInicio;

    public SistemaImp() {
	this.listaUsuarios = new ArrayList<Usuario>();
        this.listaTiposDeEvento = new ArrayList<TipoEvento>();
        this.listaCiudades = new ArrayList<Ciudad>();
	this.usuarioIdentificado = null;
	this.modoDesarrollador = false;
	this.mostrarCartelInicio = true;
    }

    @Override
    public void altaUsuario(String nombreUsuario, String clave, Email email, String nombre, String apellido) throws UsuarioException {
	if (existeNombreUsuario(nombreUsuario)) {
	    throw new UsuarioExistenteException(nombreUsuario);
	}

	Usuario nuevoUsuario = new Usuario(nombreUsuario, clave, nombre, apellido, email);
	this.listaUsuarios.add(nuevoUsuario);
    }

    @Override
    public void identificacionUsuario(String nombreUsuario, String clave) throws UsuarioException {
	boolean entro = false;
	Iterator<Usuario> it = this.listaUsuarios.iterator();

	while (it.hasNext()) {
	    Usuario usuario = it.next();
	    if (usuario.getNombreUsuario().equals(nombreUsuario)) {
		if (usuario.igualContraseña(clave)) {
		    this.usuarioIdentificado = usuario;
		    entro = true;
		} else {
		    throw new IdentificacionInvalidaException();
		}
	    }
	}

	if (!entro) {
	    throw new IdentificacionInvalidaException();
	}
    }

    @Override
    public void logoutUsuario() {
	this.usuarioIdentificado = null;
    }

    @Override
    public boolean hayUsuarioIdentificado() {
	return this.usuarioIdentificado != null;
    }

    @Override
    public void bajaUsuario() {
	if (this.hayUsuarioIdentificado()) {
	    this.listaUsuarios.remove(this.usuarioIdentificado);
	    this.logoutUsuario();
	}
    }

    @Override
    public ArrayList<Usuario> getListaUsuarios() {
	return this.listaUsuarios;
    }

    @Override
    public boolean existeUsuario(String nombreUsuario) {
	return this.listaUsuarios.contains(new Usuario(nombreUsuario));
    }

    @Override
    public Usuario getUsuarioIdentificado() {
	return this.usuarioIdentificado;
    }

    @Override
    public ArrayList<Usuario> listadoBuscarUsuarios(String cadena) {
	ArrayList<Usuario> retorno = new ArrayList<Usuario>();
	Usuario identificado = this.getUsuarioIdentificado();
	Iterator<Usuario> it = this.listaUsuarios.iterator();
	while (it.hasNext()) {
	    Usuario usuario = it.next();
	    if (!usuario.equals(identificado) && !identificado.getListaAmigos().contains(usuario)) {
		if ((usuario.getNombre() +" "+ usuario.getApellido()).toLowerCase().startsWith(cadena.toLowerCase())) {
		    retorno.add(usuario);
		}
	    }
	}
	return retorno;
    }

    @Override
    public ArrayList<Usuario> obtenerGrupoDeViaje(Viaje viaje) {
	ArrayList<Usuario> retorno = new ArrayList<Usuario>();
	retorno.add(this.getUsuarioIdentificado());

	Iterator<Usuario> it = this.getListaUsuarios().iterator();
	while (it.hasNext()) {
	    Usuario usuarioAux = it.next();
	    if (!usuarioAux.equals(this.getUsuarioIdentificado())) {
		if (usuarioAux.getListaViajes().contains(viaje)) {
		    retorno.add(usuarioAux);
		}
	    }
	}
	return retorno;
    }

    @Override
    public void setModoDesarrollador(boolean modoDesarrollador) {
	this.modoDesarrollador = modoDesarrollador;
    }

    @Override
    public boolean esModoDesarrollador() {
	return this.modoDesarrollador;
    }

    public boolean isMostrarCartelInicio() {
	return mostrarCartelInicio;
    }

    public void setMostrarCartelInicio(boolean mostrarCartelInicio) {
	this.mostrarCartelInicio = mostrarCartelInicio;
    }
    
    @Override
    public void identificar(Usuario usuario) {
	this.usuarioIdentificado = usuario;
    }

    @Override
    public void notificarNovedad(String novedadEmail, String titulo, Viaje viaje) {
	Iterator<Usuario> iterador = this.getListaUsuarios().iterator();
	while (iterador.hasNext()) {
	    Usuario usuario = iterador.next();
            if (usuario.existeNombreViaje(viaje.getNombre()) && usuario.isNotificarEmail()) {
		EnvioMail.mandarEmail(usuario.getEmail().toString(), titulo, novedadEmail);
	    }
	}
    }

    public ArrayList<String> getListaNombresDeTipoDeEvento() {
        ArrayList<String> retorno = new ArrayList();
        for (TipoEvento tipo : listaTiposDeEvento) {
            String nombreTipo = tipo.toString();
            retorno.add(nombreTipo);
        }
        return retorno;
    }
    
    private boolean existeNombreUsuario(String nombreUsuario) {
	return this.listaUsuarios.contains(new Usuario(nombreUsuario));
    }

    public TipoEvento getTipoEvento(String nombreTipoEvento) throws NoExisteTipoEventoException{
        TipoEvento nuevo = new TipoEvento(nombreTipoEvento);
        for (TipoEvento tipo : listaTiposDeEvento) {
            if(tipo.equals(nuevo)){
                return tipo;
            }
        }
        throw new NoExisteTipoEventoException();
    }

    public void setTiposDeEvento(ArrayList<TipoEvento> tipos) {
        listaTiposDeEvento = tipos;
    }
    
    public ArrayList<TipoEvento> getListaTiposDeEvento() {
        return listaTiposDeEvento;
    }

    public void addTipoDeEvento(TipoEvento tipoDeEvento) {
        this.listaTiposDeEvento.add(tipoDeEvento);
    }
    
    public void setListaTiposDeEvento(ArrayList<String> listaNombres){
        listaTiposDeEvento.clear();
        for(String nombreEvento : listaNombres){
            TipoEvento nuevoTipo = new TipoEvento(nombreEvento);
            listaTiposDeEvento.add(nuevoTipo);
        }
    }

    public void addCiudad(Ciudad ciudad) {
        this.listaCiudades.add(ciudad);
    }

    public ArrayList<Ciudad> getListaCiudades() {
        return this.listaCiudades;
    }

    public Ciudad getCiudad(String nombreCiudad) throws NoExisteCiudadException{
        Ciudad nueva = new Ciudad(nombreCiudad);
        for (Ciudad ciudad : listaCiudades) {
            if(nueva.equals(ciudad)){
                return ciudad;
            }
        }
        throw new NoExisteCiudadException();
    }

    public void setCiudades(ArrayList<Ciudad> ciudades) {
        this.listaCiudades = ciudades;
    }

}
