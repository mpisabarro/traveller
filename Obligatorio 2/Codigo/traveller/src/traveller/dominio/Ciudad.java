package traveller.dominio;

import java.io.Serializable;

public class Ciudad implements Serializable{
    private String nombre;

    public Ciudad(String uNombre){
        this.setNombre(uNombre);
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().toLowerCase().equals(((Ciudad) obj).toString().toLowerCase());
    }

    @Override
    public String toString() {
        return nombre;
    }
};

