package traveller.dominio;

import java.io.Serializable;

public class TipoEvento implements Serializable{
    private String nombreTipoEvento;

    public String getNombreTipoEvento() {
        return nombreTipoEvento;
    }

    public void setNombreTipoEvento(String unNombre) {
        this.nombreTipoEvento = unNombre;
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().toLowerCase().equals(((TipoEvento) obj).toString().toLowerCase());
    }

    public TipoEvento(String unNombre) {
        this.nombreTipoEvento = unNombre;
    }

    @Override
    public String toString() {
        return nombreTipoEvento;
    }
}
