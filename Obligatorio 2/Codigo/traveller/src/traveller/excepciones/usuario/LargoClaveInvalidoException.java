package traveller.excepciones.usuario;

public class LargoClaveInvalidoException extends UsuarioException{
    private int minimo;
    private int maximo;

    public LargoClaveInvalidoException(int minimo, int maximo) {
	this.minimo = minimo;
        this.maximo = maximo;
    }

    @Override
    public String toString() {
	return "La contraseña debe tener entre " + minimo + " y " + maximo + " caracteres.";
    }
}
