
package traveller.excepciones.usuario;

public class NombreConCaracteresEspecialesException extends UsuarioException{
    public NombreConCaracteresEspecialesException(){
    }
 
    @Override
    public String toString(){
        return "El nombre no puede tener caracteres especiales, sólo texto.";
    }
}
