
package traveller.excepciones.usuario;

public class ApellidoConCaracteresEspecialesException extends UsuarioException{
    public ApellidoConCaracteresEspecialesException(){
    }
 
    @Override
    public String toString(){
        return "El apellido no puede tener caracteres especiales, sólo texto.";
    }
}
