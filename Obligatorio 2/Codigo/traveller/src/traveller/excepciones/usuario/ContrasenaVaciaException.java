
package traveller.excepciones.usuario;

public class ContrasenaVaciaException extends UsuarioException {
    public ContrasenaVaciaException() {
    }

    @Override
    public String toString() {
	return "La contraseña no debe ser vacía.";
    }
}
