package dominio;

import traveller.dominio.Email;
import traveller.dominio.Usuario;
import traveller.dominio.SistemaImp;
import traveller.dominio.ISistema;
import traveller.excepciones.usuario.IdentificacionInvalidaException;
import traveller.excepciones.usuario.UsuarioException;
import traveller.excepciones.usuario.UsuarioExistenteException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import traveller.dominio.Ciudad;
import traveller.dominio.TipoEvento;
import traveller.excepciones.evento.NoExisteTipoEventoException;
import traveller.excepciones.viaje.NoExisteCiudadException;

public class SistemaImpTest {

    private ISistema instance;    
    private static final TipoEvento CULTURAL = new TipoEvento("Cultural");
    private static final TipoEvento DEPORTIVO = new TipoEvento("Deportivo");
    private static final TipoEvento COMERCIAL = new TipoEvento("Comercial");
    private static final TipoEvento FAMILIAR = new TipoEvento("Familiar");
    private static final TipoEvento LABORAL = new TipoEvento("Laboral");
    
    private static final Ciudad MADRID = new Ciudad("Madrid");
    private static final Ciudad PARIS = new Ciudad("Paris");
    private static final Ciudad BSAS = new Ciudad("Buenos Aires");
    private static final Ciudad MIAMI = new Ciudad("Miami");

    public SistemaImpTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        instance = new SistemaImp();
        
        instance.addTipoDeEvento(CULTURAL);
        instance.addTipoDeEvento(DEPORTIVO);
        instance.addTipoDeEvento(COMERCIAL);
        instance.addTipoDeEvento(FAMILIAR);
        instance.addTipoDeEvento(LABORAL);
        
        instance.addCiudad(MADRID);
        instance.addCiudad(PARIS);
        instance.addCiudad(BSAS);
        instance.addCiudad(MIAMI);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAltaUsuarioOK1() throws Exception {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
    }

    @Test
    public void testAltaUsuarioOK2() throws Exception {
        instance.altaUsuario("El Juancho", "juancho123", new Email("juancho@gmail.com"),
                "Juan", "Damonte");
    }

    @Test
    public void testAltaUsuarioErrorExisteUsuario() throws Exception {
        try {
            instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                    "Jose", "Damonte");
            instance.altaUsuario("Pepe", "juan123", new Email("juancho@gmail.com"),
                    "Juan", "Damonte");
            assert (false);
        } catch (UsuarioExistenteException e) {
            assert (true);
        }
    }

    @Test
    public void testIdentificacionUsuarioOK() throws Exception {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        instance.identificacionUsuario("Pepe", "pepe4000");
        assert (instance.getUsuarioIdentificado().toString().equals("Jose Damonte"));
    }

    @Test
    public void testIdentificacionUsuarioErrorInvalida1() throws Exception {
        try {
            instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                    "Jose", "Damonte");
            instance.identificacionUsuario("Pepe ", "pepe4000");
            assert (false);
        } catch (IdentificacionInvalidaException e) {
            assert (true);
        }
    }

    @Test
    public void testIdentificacionUsuarioErrorInvalida2() throws Exception {
        try {
            instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                    "Jose", "Damonte");
            instance.identificacionUsuario("Pepe", "pepe400");
            assert (false);
        } catch (IdentificacionInvalidaException e) {
            assert (true);
        }
    }

    @Test
    public void testIdentificacionUsuarioErrorInvalida3() throws Exception {
        try {
            instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                    "Jose", "Damonte");
            instance.identificacionUsuario("", "pepe4000");
            assert (false);
        } catch (IdentificacionInvalidaException e) {
            assert (true);
        }
    }

    @Test
    public void testIdentificacionUsuarioErrorInvalida4() throws Exception {
        try {
            instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                    "Jose", "Damonte");
            instance.identificacionUsuario("Pepe", "Pepe4000");
            assert (false);
        } catch (IdentificacionInvalidaException e) {
            assert (true);
        }
    }

    @Test
    public void testIdentificacionUsuarioErrorInvalida5() throws Exception {
        try {
            instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                    "Jose", "Damonte");
            instance.identificacionUsuario("pepe", "pepe4000");
            assert (false);
        } catch (IdentificacionInvalidaException e) {
            assert (true);
        }
    }

    @Test
    public void testLogoutUsuario() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        instance.identificacionUsuario("Pepe", "pepe4000");
        instance.logoutUsuario();
        assert (!instance.hayUsuarioIdentificado());
    }

    @Test
    public void testHayUsuarioIdentificadoTrue() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        instance.identificacionUsuario("Pepe", "pepe4000");
        assert (instance.hayUsuarioIdentificado());
    }

    @Test
    public void testHayUsuarioIdentificadoFalse() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        assert (!instance.hayUsuarioIdentificado());
    }

    @Test
    public void testBajaUsuario() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        instance.identificacionUsuario("Pepe", "pepe4000");
        instance.bajaUsuario();
        assert (instance.getListaUsuarios().isEmpty());
    }

    @Test
    public void testExisteUsuarioTrue() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        boolean expResult = true;
        boolean result = instance.existeUsuario("Pepe");
        assertEquals(expResult, result);
    }

    @Test
    public void testExisteUsuarioFalse() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        boolean expResult = false;
        boolean result = instance.existeUsuario("pepe");
        assertEquals(expResult, result);
    }

    @Test
    public void testGetUsuarioIdentificado() throws UsuarioException {
        instance.altaUsuario("Pepe", "pepe4000", new Email("pepeda@gmail.com"),
                "Jose", "Damonte");
        instance.identificacionUsuario("Pepe", "pepe4000");
        Usuario expResult = new Usuario("Pepe");
        Usuario result = instance.getUsuarioIdentificado();
        assertEquals(expResult, result);
    }

    @Test
    public void testListadoBuscarUsuarios() throws UsuarioException {
        Usuario u2 = new Usuario("Usuario1", "qwerty123", "Juan", "Perez", new Email("j@gmail.com"));
        Usuario u3 = new Usuario("Usuario2", "qwerty123", "Juancho", "Perez", new Email("j@gmail.com"));

        ArrayList<Usuario> expResult = new ArrayList<Usuario>();
        expResult.add(u2);
        expResult.add(u3);
        instance.altaUsuario("Usuario1", "qwerty123", new Email("j@gmail.com"),
                "Juan", "Perez");
        instance.altaUsuario("Usuario2", "qwerty123", new Email("j@gmail.com"),
                "Juancho", "Perez");
        instance.altaUsuario("Usuario", "qwerty123", new Email("peda@gmail.com"),
                "Jorge", "Damonte");
        instance.identificacionUsuario("Usuario", "qwerty123");
        ArrayList result = instance.listadoBuscarUsuarios("juan");
        assertEquals(expResult, result);
    }

    @Test
    public void testSetModoDesarrolladorTrue() {
        boolean modo = true;
        instance.setModoDesarrollador(modo);
        assertEquals(modo, instance.esModoDesarrollador());
    }

    @Test
    public void testSetModoDesarrolladorFalse() {
        boolean modo = false;
        instance.setModoDesarrollador(modo);
        assertEquals(modo, instance.esModoDesarrollador());
    }
    
    @Test
    public void testGetTipoEvento() throws NoExisteTipoEventoException{
        String nombreEvento = "Cultural";
        
        TipoEvento expectedResult = CULTURAL;
        
        TipoEvento result = instance.getTipoEvento(nombreEvento);
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetTipoEventoUpperCases() throws NoExisteTipoEventoException{
        String nombreEvento = "CuLTuraL";
        
        TipoEvento expectedResult = CULTURAL;
        
        TipoEvento result = instance.getTipoEvento(nombreEvento);
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetCiudad() throws NoExisteCiudadException{
        String nombreCiudad = "Madrid";
        
        Ciudad expectedResult = MADRID;
        
        Ciudad result = instance.getCiudad(nombreCiudad);
        
        assertEquals(expectedResult, result);
    }
    
    @Test
    public void testGetCiudadUpperCases() throws NoExisteCiudadException{
        String nombreCiudad = "MADRID";
        
        Ciudad expectedResult = MADRID;
        
        Ciudad result = instance.getCiudad(nombreCiudad);
        
        assertEquals(expectedResult, result);
    }
}
