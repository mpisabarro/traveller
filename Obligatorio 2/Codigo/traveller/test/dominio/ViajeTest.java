package dominio;

import traveller.dominio.Ciudad;
import traveller.dominio.Evento;
import traveller.dominio.Viaje;
import traveller.excepciones.comentario.ContenidoVacioException;
import traveller.excepciones.evento.EventoException;
import traveller.excepciones.evento.EventoExistenteException;
import traveller.excepciones.fecha.FechaEventoFueraViajeException;
import traveller.excepciones.fecha.FechaException;
import traveller.excepciones.fecha.FechaVaciaException;
import traveller.excepciones.fecha.FormatoFechaInicioException;
import traveller.excepciones.viaje.LargoNombreViajeInvalidoException;
import traveller.excepciones.viaje.NombreViajeVacioException;
import traveller.excepciones.viaje.ViajeException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ViajeTest {

    private Viaje instance;
    private static final Ciudad MADRID = new Ciudad("Madrid");
    private static final Ciudad BSAS = new Ciudad("BSAS");
    private static final Ciudad MIAMI = new Ciudad("Miami");

    public ViajeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws ViajeException {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaFin = null;
        try {
            fechaFin = formatter.parse(Integer.parseInt("23") + "/"
                    + Integer.parseInt("11") + "/" + Integer.parseInt("2022"));
        } catch (ParseException ex) {
        }
        instance = new Viaje("Vacaciones", MADRID,
                Calendar.getInstance().getTime(), fechaFin, "Descripcion");
    }

    @After
    public void tearDown() {
    }

     @Test
    public void testAgregarComentarioError() throws Exception {
        try {
            instance.agregarComentario("fecha1", "autor", "");
            assert (false);
        } catch (ContenidoVacioException e) {
            assert (true);
        }
    }

    @Test
    public void testExisteEventoTrue() {
        boolean expResult = true;

        instance.agregarEventos(new Evento("Evento1"));
        instance.agregarEventos(new Evento("Evento2"));
        instance.agregarEventos(new Evento("Evento3"));

        boolean result = instance.existeEvento(new Evento("Evento2"));
        assertEquals(expResult, result);
    }

    @Test
    public void testExisteEventoFalse() {
        boolean expResult = false;

        instance.agregarEventos(new Evento("Evento1"));
        instance.agregarEventos(new Evento("Evento3"));

        boolean result = instance.existeEvento(new Evento("Evento2"));
        assertEquals(expResult, result);
    }

    @Test
    public void testAltaEventoOK1() throws Exception {
        instance.altaEvento("Visita al parque", "12", "12", "2020", "Parque",
                "descripcion", "Familiar");
        assert (instance.existeEvento(new Evento("Visita al parque")));
    }

    @Test
    public void testAltaEventoOK2() throws Exception {
        instance.altaEvento("Visita al parque", "12", "12", "2020", "Parque",
                "descripcion", "Familiar");
        instance.altaEvento("Visita al parque2", "12", "12", "2020", "Parque",
                "descripcion", "Familiar");
        assert (instance.existeEvento(new Evento("Visita al parque")) && 
                instance.existeEvento(new Evento("Visita al parque2")));
    }

    @Test
    public void testAltaEventoOK3Bordes() throws Exception {
        instance.altaEvento("Park", "23", "11", "2020", "Parque",
                "descripcion", "Familiar");
        assert (instance.existeEvento(new Evento("Park")));
    }

    @Test
    public void testAltaEventoErrorNombreExistente() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "01", "01", "2021", "Aquopolis",
                    "Descripcion", "Familiar");
            instance.altaEvento("Parque Acuatico", "02", "02", "2020", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (EventoExistenteException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorDiaVacio() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "", "01", "2014", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FechaVaciaException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorMesVacio() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "02", "", "2013", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FechaVaciaException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorAñoVacio() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "02", "02", "", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FechaVaciaException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorFormatoFecha1() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "02A", "02", "2013", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FormatoFechaInicioException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorFormatoFecha2() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "02d", "13", "2013", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FormatoFechaInicioException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorFormatoFecha3() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "DD", "MM", "AAAA", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FormatoFechaInicioException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorFueraDeFecha1() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "02", "02", "2011", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FechaEventoFueraViajeException e) {
            assert (true);
        }
    }

    @Test
    public void testAltaEventoErrorFueraDeFecha2() throws Exception {
        try {
            instance.altaEvento("Parque Acuatico", "24", "11", "2014", "Aquopolis",
                    "Descripcion", "Familiar");
            assert (false);
        } catch (FechaEventoFueraViajeException e) {
            assert (true);
        }
    }

    @Test
    public void testExisteNombreEventoTrue() throws EventoException, FechaException {
        boolean expResult = true;
        instance.altaEvento("Park", "23", "11", "2020", "Parque",
                "descripcion", "Familiar");
        boolean result = instance.existeNombreEvento("Park");
        assertEquals(expResult, result);
    }

    @Test
    public void testExisteNombreEventoFalse() throws EventoException, FechaException {
        boolean expResult = false;
        instance.altaEvento("Park", "23", "11", "2020", "Parque",
                "descripcion", "Familiar");
        boolean result = instance.existeNombreEvento(" Park");
        assertEquals(expResult, result);
    }

    @Test
    public void testBajaEvento() throws EventoException, FechaException {
        Evento aux = new Evento("Evento");
        instance.agregarEventos(aux);
        instance.bajaEvento(aux);
        assert (!instance.getListaEventos().contains(aux));
    }

    @Test
    public void testAgregarEventos() {
        Evento evento1 = new Evento("Evento1");
        Evento evento2 = new Evento("Evento2");
        Evento evento3 = new Evento("Evento3");

        ArrayList<Evento> expResult = new ArrayList<Evento>();
        expResult.add(evento1);
        expResult.add(evento2);
        expResult.add(evento3);

        instance.agregarEventos(evento1);
        instance.agregarEventos(evento2);
        instance.agregarEventos(evento3);

        ArrayList result = instance.getListaEventos();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDescripcion() {
        String expResult = "Descripcion";
        String result = instance.getDescripcion();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetDescripcion() {
        String descripcion = "Nueva descripcion";
        instance.setDescripcion(descripcion);
        assertEquals(descripcion, instance.getDescripcion());
    }

    @Test
    public void testGetDestino() {
        Ciudad expResult = MADRID;
        Ciudad result = instance.getDestino();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetDestino() {
        Ciudad destino = MADRID;
        instance.setDestino(destino);
        assertEquals(destino, instance.getDestino());
    }

    @Test
    public void testGetFechaFin() {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date expResult = null;
        try {
            expResult = formatter.parse(Integer.parseInt("23") + "/" + Integer.parseInt("11")
                    + "/" + Integer.parseInt("2022"));
        } catch (ParseException ex) {
        }

        Date result = instance.getFechaFin();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetFechaFin() {
        Date fechaFin = Calendar.getInstance().getTime();
        instance.setFechaFin(fechaFin);
        assertEquals(fechaFin, instance.getFechaFin());
    }

    @Test
    public void testGetFechaInicio() {
        Date expResult = Calendar.getInstance().getTime();
        Date result = instance.getFechaInicio();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetFechaInicio() {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaInicio = null;
        try {
            fechaInicio = formatter.parse(Integer.parseInt("23") + "/" + Integer.parseInt("11")
                    + "/" + Integer.parseInt("2014"));
        } catch (ParseException ex) {
        }
        instance.setFechaInicio(fechaInicio);
        assertEquals(fechaInicio, instance.getFechaInicio());
    }

    @Test
    public void testGetNombre() {
        String expResult = "Vacaciones";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetNombre() throws Exception {
        String nombre = "Negocio";
        instance.setNombre(nombre);
        assertEquals(nombre, instance.getNombre());
    }

    @Test
    public void testSetNombreErrorVacio() throws Exception {
        try {
            String nombre = "";
            instance.setNombre(nombre);
            assert (false);
        } catch (NombreViajeVacioException e) {
            assert (true);
        }
    }

    @Test
    public void testSetNombreErrorMuyCorto1() throws Exception {
        try {
            String nombre = "V";
            instance.setNombre(nombre);
            assert (false);
        } catch (LargoNombreViajeInvalidoException e) {
            assert (true);
        }
    }

    @Test
    public void testSetNombreErrorMuyCorto2() throws Exception {
        try {
            String nombre = "vje";
            instance.setNombre(nombre);
            assert (false);
        } catch (LargoNombreViajeInvalidoException e) {
            assert (true);
        }
    }

    @Test
    public void testSetNombreErrorMuyLargo1() throws Exception {
        try {
            String nombre = "Nombre de viaje muy largo para validar";
            instance.setNombre(nombre);
            assert (false);
        } catch (LargoNombreViajeInvalidoException e) {
            assert (true);
        }
    }

    @Test
    public void testSetNombreErrorMuyLargo2() throws Exception {
        try {
            String nombre = "Nombre de 21 caracter";
            instance.setNombre(nombre);
            assert (false);
        } catch (LargoNombreViajeInvalidoException e) {
            assert (true);
        }
    }

    @Test
    public void testEqualsOK() throws ViajeException {
        Object obj = new Viaje("Vacaciones", MADRID, Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime(), "Descripcion");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    @Test
    public void testEqualsError1() throws ViajeException {
        Object obj = new Viaje("VACACIONES", MADRID, Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime(), "Descripcion");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    @Test
    public void testEqualsError2() throws ViajeException {
        Object obj = new Viaje("Negocio", BSAS, Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime(), "Descripcion");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    @Test
    public void testToStringError() {
        String expResult = "Vacaciones";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
